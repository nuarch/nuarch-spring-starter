package com.nuarch.spring.starters.featureflags;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.util.StringUtils;

public class FeatureFlagsFactoryImpl implements FeatureFlagsFactory {

    @Autowired
    private Environment env;

    public boolean isEnabled(String feature) {

        if (StringUtils.isEmpty(feature)) {
            return false;
        }

        return Boolean.parseBoolean(env.getProperty(feature));
    }
}
