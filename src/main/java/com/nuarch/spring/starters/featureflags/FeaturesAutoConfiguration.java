package com.nuarch.spring.starters.featureflags;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnResource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ConditionalOnResource(resources = "classpath:features.yml")
@PropertySource("classpath:features.yml")
@ConfigurationProperties(prefix="feature")
public class FeaturesAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public FeatureFlagsFactory featureFlagsFactory(){

        return new FeatureFlagsFactoryImpl();
    }
}

