package com.nuarch.spring.starters.featureflags;

public interface FeatureFlagsFactory {

    boolean isEnabled(String feature);
}
