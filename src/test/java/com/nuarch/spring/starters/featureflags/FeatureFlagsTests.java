package com.nuarch.spring.starters.featureflags;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = FeaturesAutoConfiguration.class)
class FeatureFlagsTests {

	@Autowired
	FeatureFlagsFactory featureFlagsFactory;

	@Test
	void contextLoads() {
		assertThat(featureFlagsFactory).isNotNull();
	}

	@Test
	void shouldCheckFeaturesAreEnabled() {
		assertThat(featureFlagsFactory.isEnabled("LoginWithFacebook")).isTrue();
		assertThat(featureFlagsFactory.isEnabled("LoginWithApple")).isTrue();
		assertThat(featureFlagsFactory.isEnabled("DarkThemeSupport")).isTrue();
	}

	@Test
	void shouldCheckFeaturesAreDisabled() {
		assertThat(featureFlagsFactory.isEnabled("NewYearThemeSupport")).isFalse();
	}

	@Test
	void shouldCheckFeaturesAreNotBooleanValueAndDisabled() {
		assertThat(featureFlagsFactory.isEnabled("MaxUserNameLength")).isFalse();
	}

	@Test
	void shouldCheckFeaturesAreEmptyValueAndDisabled() {
		assertThat(featureFlagsFactory.isEnabled("ValidateEmailByLibrary")).isFalse();
	}

	@Test
	void shouldCheckFeaturesAreMissedAndDisabled() {
		assertThat(featureFlagsFactory.isEnabled("MissedInConfigurationFile")).isFalse();
	}
}
