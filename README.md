# nuArch Spring Starter

## Installation
Add the following dependency to your `build.gradle` file:

```
dependencies {
    implementation 'com.nuarch.spring.starters:feature-flags:0.0.1-SNAPSHOT'
}
```
## Usage
1. Create `features.yml` file in `resources` directory
2. Add features to `features.yml` file

```yaml
feature:
  LoginWithApple: true
  DarkThemeSupport: true
  LoginWithFacebook: true
  NewYearThemeSupport: false
```

**Note that the prefix for filtered features is `feature`**

Check from code:  

```java
@Autowired
FeatureFlagsFactory featureFlagsFactory;

...

if (featureFlagsFactory.isEnabled("DarkThemeSupport")) {
    // do some stuff 
}
```